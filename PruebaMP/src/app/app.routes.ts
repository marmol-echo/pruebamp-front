import { Routes } from '@angular/router';
import {CrudCompanyComponent} from "./components/crud-company/crud-company.component";
import {AppComponent} from "./app.component";

export const routes: Routes = [
  { path: '', component : AppComponent },
  { path: 'crud', component : CrudCompanyComponent }
];
