import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {CountryListResponse} from "../classes/country-list-response";
import {CompanyListResponse} from "../classes/company-list-response";
import {CompanyRequest} from "../classes/company-request";

@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  private host : string = "http://localhost:8090/prueba/";
  private findCountries : string = "getCountries";
  private findCompanies : string = "getCompanies";
  private create : string = "saveCompany";
  private delete : string = "deleteCompany";

  constructor(private client : HttpClient) { }

  listCountries() : Observable<CountryListResponse> {
    return this.client.get<CountryListResponse>(this.host + this.findCountries);
  }

  listCompanies(id : number, name : string) : Observable<CompanyListResponse> {
    if (id != null)
      return this.client.get<CompanyListResponse>(this.host + this.findCompanies + "?id=" + id);
    else if (name != null)
      return this.client.get<CompanyListResponse>(this.host + this.findCompanies + "?name=" + name);
    else
      return this.client.get<CompanyListResponse>(this.host + this.findCompanies);
  }

  saveCompany(com : CompanyRequest) : Observable<Object> {
    com.taxCode = com.taxCode.toUpperCase();
    return this.client.post<CompanyRequest>(this.host + this.create, com);
  }

  deleteCompany(id : number) : Observable<Object> {
    if (id != null)
      return this.client.delete<CompanyRequest>(this.host + this.delete + "?id=" + id);
    else return null;
  }

}
