import {Country} from "./country";

export class Company {

  companyId : number;
  companyName : string;
  companyTaxcode : string;
  countryId : Country;

  constructor() {
    this.countryId = new Country();
  }

}
