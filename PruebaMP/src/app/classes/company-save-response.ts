import {Company} from "./company";

export class CompanySaveResponse {

  msg : string;
  code: number;
  result : Company;

}
