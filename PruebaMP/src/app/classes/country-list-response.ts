import {Country} from "./country";

export class CountryListResponse {

  msg : string;
  code: number;
  result : Country[];

}
