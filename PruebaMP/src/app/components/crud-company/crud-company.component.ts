import {Component, OnInit, Input, model} from '@angular/core';
import {Company} from "../../classes/company";
import { CompanyService } from "../../service/company.service";
import {NgForOf, NgIf} from "@angular/common";
import {RouterLink} from "@angular/router";
import {Country} from "../../classes/country";
import {CompanyRequest} from "../../classes/company-request";
import {CompanySaveResponse} from "../../classes/company-save-response";
import {FormsModule} from "@angular/forms";

@Component({
  selector: 'app-crud-company',
  standalone: true,
  imports: [NgForOf, RouterLink, FormsModule, NgIf],
  templateUrl: './crud-company.component.html',
  styleUrl: './crud-company.component.css'
})
export class CrudCompanyComponent implements OnInit {

  companyList : Company[];
  countryList : Country[];
  @Input() request : CompanyRequest;
  @Input() country : Country;
  @Input() newCompany : Company;
  @Input() editCompany : Company;
  @Input() countryId : number;
  @Input() companyId : number;

  nameInputTitle : string = "Ingrese el nombre de la compañía...";
  countrySelectTitle : string = "Seleccione un país...";
  taxCodeInputTitle : string = "Ingrese el número tributario según el país...";

  constructor(private service : CompanyService) {
  }

  ngOnInit(): void {
    if (this.countryList == null)   this.getCountries();
    this.getCompanies(null, null);
    this.companyId = 0;
    this.countryId = 0;
    this.newCompany = new Company();
    this.editCompany = new Company();
    this.request = new CompanyRequest();
  }

  private getCompanies(id : number, name : string) {
    this.service.listCompanies(id,name)
      .subscribe(data => {
        this.companyList = data.result;
      }
    )
  }

  private getCountries() {
    this.service.listCountries()
      .subscribe(data => {
        this.countryList = data.result;
      }
    );
  }

  save() {
    this.request.companyName = this.newCompany.companyName;
    this.request.taxCode = this.newCompany.companyTaxcode;
    this.request.countryId = this.countryId;

    this.saveRequest();
  }

  edit() {
    this.request.companyId = this.editCompany.companyId;
    this.request.companyName = this.editCompany.companyName;
    this.request.taxCode = this.editCompany.companyTaxcode;
    this.request.countryId = this.countryId;

    this.saveRequest();
  }

  saveRequest() {
    this.service.saveCompany(this.request)
      .subscribe(data => {
          console.log((<CompanySaveResponse> data))
          if ((<CompanySaveResponse> data).code != 0)
            window.alert("Ha ocurrido un error")
          else
            window.location.reload();
        }
      );

    window.confirm('Se ha guardado la compañía');
    window.location.reload();
  }

  delete() {
    if (this.companyId != null && this.companyId > 0)
      this.service.deleteCompany(this.companyId).subscribe(
        data => {
          console.log((<CompanySaveResponse> data))

          if ((<CompanySaveResponse> data).code != 0)
            window.alert("Ha ocurrido un error")
          else
            window.location.reload();
        }
      );
    else
      this.companyId = 0;
  }

  convert(c : any) {
    this.editCompany = (<Company> c);
    this.request.countryId = this.editCompany.countryId.countryId;
  }

  // @ts-ignore
  selectPattern(): string {
    if (this.countryId > 0) {
      switch (this.countryId.toString()) {
        case '1': // Guatemala
          return '^[0-9]{9}$';
        case '2': // El Salvador
          return '^\\d{8}-\\d$';
        case '3': // Honduras
          return '^\\d{4}-\\d{6}-\\d{3}-\\d$';
        case '4': // Mexico
          return '^[A-Za-z]{4}-\\d{6}-[A-Za-z]{2}-\\d$';
      }
    }

    return '';
  }

  // @ts-ignore
  selectPlaceholder(): string {
    if (this.countryId > 0) {
      switch (this.countryId.toString()) {
        case '1': // Guatemala
          return '#########';
        case '2': // El Salvador
          return '########-#';
        case '3': // Honduras
          return '####-######-###-#';
        case '4': // Mexico
          return 'AAAA-######-AA-#';
      }
    }

    return '';
  }

}
